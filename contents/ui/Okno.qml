import QtQuick 2.0
import QtQuick.Layouts 1.2

Column {
    id: root

    property real lineHeight: units.gridUnit * 2
    property var config: JSON.parse(plasmoid.configuration.currencies)

    Layout.minimumWidth: units.gridUnit * 10
    Layout.minimumHeight: lineHeight * lines.count

    Repeater {
        id: lines
        model: config
        delegate: Currency {
            width: root.width
            height: root.lineHeight
            
            // Old-style configuration used only one item: "symbol":"USD_EUR"
            property string oldConfig: modelData.symbol
            pairName: oldConfig ? oldConfig : modelData.pairName
            from: oldConfig ? oldConfig.split('_')[0] : modelData.from
            to: oldConfig ? oldConfig.split('_')[1] : modelData.to
        }
    }
}

