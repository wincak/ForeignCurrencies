import QtQuick 2.0
import QtQuick.Layouts 1.2
import QtQuick.Controls 1.4
import org.kde.plasma.plasmoid 2.0
import org.kde.plasma.core 2.0 as PlasmaCore

Item {
    id: root;

    property string pairName
    property string from
    property string to

    MouseArea {
        anchors.fill: root;
        onClicked: {
            var suffix = root.from + "+in+" + root.to + "&t=canonical&ia=currency"
            Qt.openUrlExternally("https://duckduckgo.com/?q=" + suffix)
        }
    }

    Text  {
        id: currency;
        text: root.pairName;
        font.pixelSize: 18;
        color: theme.textColor;
        height: font.pixelsize*1.1;
        width: parent.width * 0.5;
        horizontalAlignment: Text.AlignLeft;
        verticalAlignment: Text.AlignVCenter;
        anchors.top: parent.top;
        anchors.bottom: parent.bottom;
        anchors.left: parent.left;
    }

    Text  {
        id: price;
        text: "unknown";
        font.pixelSize: 18;
        color: theme.textColor;
        height: font.pixelsize*1.1;
        width: parent.width * 0.5;
        horizontalAlignment: Text.AlignRight;
        verticalAlignment: Text.AlignVCenter;
        anchors.top: parent.top;
        anchors.bottom: parent.bottom;
        anchors.right: parent.right;
        anchors.left: currency.right;
    }

    Timer {
        id: timer;
        interval: 50;
        running: true;
        repeat: true;
        onTriggered: {
            console.log("=== Request attempt ===");
            if ((root.from === "") || (root.to === "")) {
                console.log("Empty parameter(s)");
                timer.interval = 10000;
                return;
            }
            var date = new Date();
            var rq = new XMLHttpRequest();
      
            // https://api.frankfurter.app/latest?base=USD&symbols=JPY
            var url = "https://api.frankfurter.app/latest?base=" + root.from + "&symbols=" + root.to;
            console.log("Requesting exchange rate from URL:", url);
            
            rq.onreadystatechange = function() {
                // readyState goes through states before request is complete.
                if (rq.readyState != XMLHttpRequest.DONE)
                    return;
                
                // Status 200 means OK.
                if (rq.status == 200) {
                    console.log("Http response:", rq.responseText)
                    var jsonObject = eval('(' + rq.responseText + ')');
                    // Http response: {"amount":1.0,"base":"USD","date":"2019-04-12","rates":{"EUR":0.88331}}
                    price.text = jsonObject["rates"][root.to];
                } else {
                    console.log("Http request error:", rq.status)
                }
            }
           
            rq.open("GET", url, true);
            rq.send();
            timer.interval = 900000
        }
    }

}

